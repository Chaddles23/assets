# Helper SASS/SCSS/JS classes/mixins/functions/etc
Requires Laravel Mix

## Installation

```
#!bash

npm install mik-assets -S
```

## Usage

sass/scss
```
#!sass

@import "~mik-assets/assets/stylesheets";
```
js
```
#!javascript

require('mik-assets');
```

## Compiling

```
#!bash

npm run watch
```

###Styling Helpers
In this suite is a set of sweet CSS/SASS helper classes/functions.
They must be manually imported into your SCSS.
```
#!scss
@import "packages/mik-assets/mik-assets";
```

There is also a set of colour classes defined in this suite as defined below:
```
#!scss
$colours: (
    "clear" : transparent,
    "black" : #000000,
    "white" : #FFFFFF,
);
```
which **NEEDS** to be defined before including the main SASS file above.
These will create a set of classes that will apply colour changes to their elements.
```
.text-COLOUR
.text-hover-COLOUR
.bg-COLOUR
.bg-hover-COLOUR
.border-COLOUR
.border-hover-COLOUR
```
All of these classes can also be applied for different the different **bootstrap** screen sizes:
```
.text-COLOUR-xs-up
.text-COLOUR-sm-up
.text-COLOUR-md-up
.text-COLOUR-lg-up
```
*they are only UP to enforce mobile first development*
