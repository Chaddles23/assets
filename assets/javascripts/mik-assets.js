// send multiple events for multiple codes
function send_event(category, action, label, value) {
    try {
        var trackers = ga.getAll();
        for (var i = 0; i < trackers.length; i++) {
            var trackingId = trackers[i].get('trackingId');
            gtag('event', action, {
                'send_to': trackingId,
                'event_category': category,
                'event_label': label,
                'value': value
            });
        }
    } catch (err) {
        console.log(err.message);
    }
}
window.send_event = send_event;

function meta(name) {
    return document.head.querySelector("[name=" + name + "]") ? document.head.querySelector("[name=" + name + "]").content : null;
}
window.meta = meta;

function loadImage(image, callback) {
    var img = new Image();
    img.src = image;
    img.onload = callback;
}
window.loadImage = loadImage;

function isMobile() {
    if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ) {
        return true;
    }
    else {
        return false;
    }
}
window.isMobile = isMobile;

jQuery.fn.scrollToElem = function (elem, speed) {
    $(this).animate({
        scrollTop: $(this).scrollTop() - $(this).offset().top + elem.offset().top
    }, speed == undefined ? 1000 : speed);
    return this;
};

$(function () {
    // fire onload events
    $('div[onload]').trigger('onload');

    //Animate scroll to # href's
    $('a[href*=\\#]:not([href=\\#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('body').on('click', '.button.disabled', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    $('body').on('click', 'label a', function (e) {
        e.stopPropagation();
    });
});
